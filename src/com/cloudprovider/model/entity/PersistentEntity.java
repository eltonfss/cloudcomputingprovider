package com.cloudprovider.model.entity;

import java.io.Serializable;

public interface PersistentEntity extends Serializable {

	public Integer getId();

}
