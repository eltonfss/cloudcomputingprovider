package com.cloudprovider.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "machine_type")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class MachineType implements PersistentEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_machine_type")
	private Integer id;
	@Column(name = "name",  length = 255, nullable = false)
	private String name;
	@Column(name = "virtual_cpus", nullable = false)
	private Integer virtualCPUS;
	@Column(name = "memory", nullable = false)
	private Double memory;
	@Column(name = "max_number_of_persistent_disks", nullable = false)
	private Integer maxNumberOfPersistentDisks;
	@Column(name = "max_total_persistent_disk_size", nullable = false)
	private Double maxTotalPersistentDiskSize;

	public MachineType() {
	}

	public MachineType(Integer id, String name, Integer virtualCPUS, Double memory, Integer maxNumberOfPersistentDisks,
			Double maxTotalPersistentDiskSize) {
		super();
		this.id = id;
		this.name = name;
		this.virtualCPUS = virtualCPUS;
		this.memory = memory;
		this.maxNumberOfPersistentDisks = maxNumberOfPersistentDisks;
		this.maxTotalPersistentDiskSize = maxTotalPersistentDiskSize;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getVirtualCPUS() {
		return virtualCPUS;
	}

	public void setVirtualCPUS(Integer virtualCPUS) {
		this.virtualCPUS = virtualCPUS;
	}

	public Double getMemory() {
		return memory;
	}

	public void setMemory(Double memory) {
		this.memory = memory;
	}

	public Integer getMaxNumberOfPersistentDisks() {
		return maxNumberOfPersistentDisks;
	}

	public void setMaxNumberOfPersistentDisks(Integer maxNumberOfPersistentDisks) {
		this.maxNumberOfPersistentDisks = maxNumberOfPersistentDisks;
	}

	public Double getMaxTotalPersistentDiskSize() {
		return maxTotalPersistentDiskSize;
	}

	public void setMaxTotalPersistentDiskSize(Double maxTotalPersistentDiskSize) {
		this.maxTotalPersistentDiskSize = maxTotalPersistentDiskSize;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	public Integer getId() {
		return id;
	}

}
