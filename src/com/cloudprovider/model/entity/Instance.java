package com.cloudprovider.model.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "instance")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Instance implements PersistentEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_instance")
	private Integer id;

	@Column(name = "description",  length = 255, nullable = false)
	private String description;

	@Column(name = "hostname", length = 255, nullable = false)
	private String hostname;

	@Column(name = "name", length = 255, nullable = false)
	private String name;

	@Column(name = "on_host_maintenance")
	private Boolean onHostMaintenance;

	@Column(name = "automatic_restart")
	private Boolean automaticRestart;

	@Column(name = "preemtible")
	private final Boolean preemtible;

	@ManyToMany
	@JoinTable(name = "instance_ssh_key", joinColumns = { @JoinColumn(name = "id_instance") }, inverseJoinColumns = {
			@JoinColumn(name = "id_ssh_key") })
	private List<SSHKey> sshKeys;

	@ManyToOne
	@JoinColumn(name = "id_machine_type")
	private MachineType machineType;

	@ManyToMany
	@JoinTable(name = "instance_disk", joinColumns = { @JoinColumn(name = "id_instance") }, inverseJoinColumns = {
			@JoinColumn(name = "id_disk") })
	private List<Disk> disks;

	@ManyToMany
	@JoinTable(name = "instance_network_interface", joinColumns = {
			@JoinColumn(name = "id_instance") }, inverseJoinColumns = { @JoinColumn(name = "id_network_interface") })
	private List<NetworkInterface> networkInterface;

	@ManyToOne
	@JoinColumn(name = "id_zone")
	private Zone zone;

	public Instance() {
		preemtible = false;
	}

	public Instance(Boolean preemtible) {
		super();
		this.preemtible = preemtible;
	}

	public Instance(Integer id, String description, String hostname, String name, Boolean onHostMaintenance,
			Boolean automaticRestart, Boolean preemtible, List<SSHKey> sshKeys, MachineType machineType,
			List<Disk> disks, List<NetworkInterface> networkInterface, Zone zone) {
		super();
		this.id = id;
		this.description = description;
		this.hostname = hostname;
		this.name = name;
		this.onHostMaintenance = onHostMaintenance;
		this.automaticRestart = automaticRestart;
		this.preemtible = preemtible;
		this.sshKeys = sshKeys;
		this.machineType = machineType;
		this.disks = disks;
		this.networkInterface = networkInterface;
		this.zone = zone;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Zone getZone() {
		return zone;
	}

	public void setZone(Zone zone) {
		this.zone = zone;
	}

	public List<SSHKey> getSshKeys() {
		return sshKeys;
	}

	public void setSshKeys(List<SSHKey> sshKeys) {
		this.sshKeys = sshKeys;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getOnHostMaintenance() {
		return onHostMaintenance;
	}

	public void setOnHostMaintenance(Boolean onHostMaintenance) {
		this.onHostMaintenance = onHostMaintenance;
	}

	public Boolean getAutomaticRestart() {
		return automaticRestart;
	}

	public void setAutomaticRestart(Boolean automaticRestart) {
		this.automaticRestart = automaticRestart;
	}

	public MachineType getMachineType() {
		return machineType;
	}

	public void setMachineType(MachineType machineType) {
		this.machineType = machineType;
	}

	public List<Disk> getDisks() {
		return disks;
	}

	public void setDisks(List<Disk> disks) {
		this.disks = disks;
	}

	public List<NetworkInterface> getNetworkInterfaces() {
		return networkInterface;
	}

	public void setNetworkInterface(List<NetworkInterface> networkInterface) {
		this.networkInterface = networkInterface;
	}

	public Boolean getPreemtible() {
		return preemtible;
	}

}
