package com.cloudprovider.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "network_interface")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class NetworkInterface implements PersistentEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_network_interface")
	private Integer id;
	@Column(name = "mac_address", length = 255, nullable = false)
	private String MACAddress;
	@Column(name = "description", length = 255)
	private String description;
	@Column(name = "private_ipv4_address", nullable = false)
	private String privateIPv4Address;
	@Column(name = "public_ipv4_address", nullable = false)
	private String publicIPv4Address;
	@Column(name = "private_ipv6_address", nullable = false)
	private String privateIPv6Address;
	@Column(name = "public_ipv6_address", nullable = false)
	private String publicIPv6Address;

	public NetworkInterface() {
	}

	public NetworkInterface(Integer id, String mACAddress, String description, String privateIPv4Address,
			String publicIPv4Address, String privateIPv6Address, String publicIPv6Address) {
		super();
		this.id = id;
		MACAddress = mACAddress;
		this.description = description;
		this.privateIPv4Address = privateIPv4Address;
		this.publicIPv4Address = publicIPv4Address;
		this.privateIPv6Address = privateIPv6Address;
		this.publicIPv6Address = publicIPv6Address;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMACAddress() {
		return MACAddress;
	}

	public void setMACAddress(String mACAddress) {
		MACAddress = mACAddress;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPrivateIPv4Address() {
		return privateIPv4Address;
	}

	public void setPrivateIPv4Address(String privateIPv4Address) {
		this.privateIPv4Address = privateIPv4Address;
	}

	public String getPublicIPv4Address() {
		return publicIPv4Address;
	}

	public void setPublicIPv4Address(String publicIPv4Address) {
		this.publicIPv4Address = publicIPv4Address;
	}

	public String getPrivateIPv6Address() {
		return privateIPv6Address;
	}

	public void setPrivateIPv6Address(String privateIPv6Address) {
		this.privateIPv6Address = privateIPv6Address;
	}

	public String getPublicIPv6Address() {
		return publicIPv6Address;
	}

	public void setPublicIPv6Address(String publicIPv6Address) {
		this.publicIPv6Address = publicIPv6Address;
	}

}
