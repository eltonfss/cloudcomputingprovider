package com.cloudprovider.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "disk")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Disk implements PersistentEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_disk")
	private Integer id;

	@Column(name = "name",  length = 255, nullable = false)
	private String name;

	@Column(name = "capacity", precision = 2, nullable = false)
	private Double capacity;

	public Disk() {
	}

	public Disk(Integer id, String name, Double capacity) {
		super();
		this.id = id;
		this.name = name;
		this.capacity = capacity;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getCapacity() {
		return capacity;
	}

	public void setCapacity(Double capacity) {
		this.capacity = capacity;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	public Integer getId() {
		return id;
	}

}
