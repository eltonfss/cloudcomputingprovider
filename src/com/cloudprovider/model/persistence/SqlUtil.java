package com.cloudprovider.model.persistence;

import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.cfg.Configuration;
import org.hibernate.tool.hbm2ddl.SchemaExport;

public class SqlUtil {

	public static void main(String[] args) {

		Configuration cfg = new AnnotationConfiguration().configure("META-INF/hibernate.cfg.xml");
		new SchemaExport(cfg).create(true, true);

	}

}
