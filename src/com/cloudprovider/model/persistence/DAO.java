package com.cloudprovider.model.persistence;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.AbstractQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Root;

import org.hibernate.Hibernate;
import org.hibernate.ejb.criteria.CriteriaQueryImpl;

import com.cloudprovider.model.entity.PersistentEntity;

/**
 * Class that works as a generic Data Acess Object with CRUD functions
 * 
 */
public class DAO<EntityClass> {

	private final String PERSISTENT_UNIT_NAME = "cloud_computing_provider";

	private EntityManagerFactory entityManagerFactory;
	@PersistenceContext(unitName = PERSISTENT_UNIT_NAME)
	private EntityManager entityManager;

	public DAO() {
		super();
		entityManagerFactory = Persistence.createEntityManagerFactory(PERSISTENT_UNIT_NAME);
		entityManager = entityManagerFactory.createEntityManager();
	}

	public void create(EntityClass entity) {
		Hibernate.initialize(entity);
		this.entityManager.getTransaction().begin();
		this.entityManager.persist(entity);
		this.entityManager.flush();
		this.entityManager.getTransaction().commit();
	}

	public void remove(EntityClass entity) {
		this.entityManager.getTransaction().begin();
		this.entityManager.remove(entityManager.getReference(entity.getClass(), ((PersistentEntity) entity).getId()));
		this.entityManager.flush();
		this.entityManager.getTransaction().commit();

	}

	public void edit(EntityClass entity) {
		this.entityManager.getTransaction().begin();
		this.entityManager.merge(entity);
		this.entityManager.flush();
		this.entityManager.getTransaction().commit();

	}

	public EntityClass findById(Class<EntityClass> type, Object id) {
		this.entityManager.clear();
		this.entityManager.getTransaction().begin();
		EntityClass entity = this.entityManager.find(type, id);
		this.entityManager.getTransaction().commit();
		return entity;

	}

	public List<EntityClass> findAll(Class<EntityClass> type) {
		this.entityManager.clear();
		// Open transaction
		this.entityManager.getTransaction().begin();
		this.entityManager.flush();
		// Get Criteria query editor
		CriteriaBuilder criteriaBuilder = this.entityManagerFactory.getCriteriaBuilder();

		// Create Criteria query
		CriteriaQueryImpl<EntityClass> criteriaQuery = (CriteriaQueryImpl<EntityClass>) criteriaBuilder
				.createQuery(type);

		// Get table root
		Root<EntityClass> root = ((AbstractQuery<EntityClass>) criteriaQuery).from(type);

		// Retrieve records
		CriteriaQueryImpl<EntityClass> records = (CriteriaQueryImpl<EntityClass>) criteriaQuery.select(root);

		// Retrieve query of all records
		TypedQuery<EntityClass> queryOfAllRecords = (TypedQuery<EntityClass>) this.entityManager.createQuery(records);

		// Committ transaction
		this.entityManager.getTransaction().commit();

		return queryOfAllRecords.getResultList();
	}

	public void removeAll(Class<EntityClass> type) {

		// Open transaction
		this.entityManager.getTransaction().begin();

		// Get Criteria query editor
		CriteriaBuilder criteriaBuilder = this.entityManagerFactory.getCriteriaBuilder();

		// Create Criteria query
		CriteriaQueryImpl<EntityClass> criteriaQuery = (CriteriaQueryImpl<EntityClass>) criteriaBuilder
				.createQuery(type);

		// Get table root
		Root<EntityClass> root = ((AbstractQuery<EntityClass>) criteriaQuery).from(type);

		// Retrieve records
		CriteriaQueryImpl<EntityClass> records = (CriteriaQueryImpl<EntityClass>) criteriaQuery.select(root);

		// Retrieve query of all records
		TypedQuery<EntityClass> queryOfAllRecords = (TypedQuery<EntityClass>) this.entityManager.createQuery(records);

		// Remover all records
		for (EntityClass entity : queryOfAllRecords.getResultList()) {
			this.entityManager
					.remove(entityManager.getReference(entity.getClass(), ((PersistentEntity) entity).getId()));
		}

		this.entityManager.flush();

		// Committ transaction
		this.entityManager.getTransaction().commit();

	}
}
