package com.cloudprovider.resource;

import java.net.URI;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.cloudprovider.model.entity.Instance;
import com.cloudprovider.model.persistence.DAO;

@Path("instance")
public class InstanceResource {

	@Path("{id}")
	@GET
	@Produces(MediaType.APPLICATION_XML)
	public Instance findById(@PathParam("id") Integer id) {
		Instance instance = new DAO<Instance>().findById(Instance.class, id);
		return instance;
	}

	@POST
	@Consumes(MediaType.APPLICATION_XML)
	public Response create(Instance instance) {
		new DAO<Instance>().create(instance);
		URI location = URI.create("instance/" + instance.getId());
		return Response.created(location).build();
	}

	@Path("{id}")
	@DELETE
	public Response remove(@PathParam("id") Integer id) {
		DAO<Instance> dao = new DAO<Instance>();
		Instance instance = new Instance();
		instance.setId(id);
		dao.remove(instance);
		return Response.ok().build();
	}

	@Path("{id}")
	@PUT
	@Consumes(MediaType.APPLICATION_XML)
	public Response edit(@PathParam("id") Integer id, Instance instance) {
		new DAO<Instance>().edit(instance);
		return Response.ok().build();
	}

}
