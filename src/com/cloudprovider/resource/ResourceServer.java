package com.cloudprovider.resource;

import java.io.IOException;
import java.net.URI;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

public class ResourceServer {

	public static HttpServer startServer() {
		ResourceConfig config = new ResourceConfig().packages("com.cloudprovider");
		URI uri = URI.create("http://localhost:8080/");
		HttpServer server = GrizzlyHttpServerFactory.createHttpServer(uri, config);
		return server;
	}

	public static void main(String[] args) throws IOException {
		HttpServer server = startServer();
		System.in.read();
		server.stop();
	}

}
