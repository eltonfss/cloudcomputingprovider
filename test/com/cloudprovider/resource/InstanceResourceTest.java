package com.cloudprovider.resource;

import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.filter.LoggingFilter;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.cloudprovider.TestData;
import com.cloudprovider.model.entity.Disk;
import com.cloudprovider.model.entity.Instance;
import com.cloudprovider.model.entity.NetworkInterface;
import com.cloudprovider.model.entity.SSHKey;
import com.cloudprovider.model.persistence.DAO;

public class InstanceResourceTest {

	private static HttpServer server;
	private static WebTarget target;
	private static Client client;
	private static TestData testData;

	@BeforeClass
	public static void initializeTestData() {
		testData = new TestData();
		testData.createDisks();
		testData.createMachineType();
		testData.createNetworkInterfaces();
		testData.createSSHKeys();
		testData.createZone();
	}

	@BeforeClass
	public static void startServer() {
		server = ResourceServer.startServer();
		ClientConfig config = new ClientConfig();
		config.register(new LoggingFilter());
		client = ClientBuilder.newClient(config);
		target = client.target("http://localhost:8080");
	}

	@AfterClass
	public static void stopServer() {
		server.stop();
	}

	@Test
	public void createTest() {
		Instance instance = testData.getNewInstance();
		Entity<Instance> entity = Entity.entity(instance, MediaType.APPLICATION_XML);
		Response response = target.path("/instance").request().post(entity);
		Assert.assertEquals(201, response.getStatus());
	}

	@Test
	public void updateTest() {
		testData.createInstance();
		Instance instance = testData.getInstance();

		Integer id = instance.getId();

		testData.createDisks();
		testData.createMachineType();
		testData.createNetworkInterfaces();
		testData.createSSHKeys();
		testData.createZone();
		instance = testData.getNewInstance();
		instance.setId(id);

		Entity<Instance> entity = Entity.entity(instance, MediaType.APPLICATION_XML);
		Response response = target.path("/instance/" + id).request().put(entity);
		Assert.assertEquals(200, response.getStatus());

		// seach instance by id
		Instance retrievedInstance = new DAO<Instance>().findById(Instance.class, id);
		assertEquals(instance, retrievedInstance);
	}

	@Test
	public void removeTest() {

		testData.removeAllInstancesFromDatabase();

		// insert 10 instances
		for (int i = 0; i < 10; i++) {
			testData.createInstance();
		}

		// get all instances from database
		DAO<Instance> instanceDAO = new DAO<Instance>();
		List<Instance> instances = instanceDAO.findAll(Instance.class);

		for (Instance instance : instances) {

			// remove instance by web service
			Response response = target.path("/instance/" + instance.getId()).request().delete();
			Assert.assertEquals(200, response.getStatus());

			// seach instance by id
			Instance retrievedInstance = instanceDAO.findById(Instance.class, instance.getId());
			Assert.assertNull(retrievedInstance);
		}
	}

	@Test
	public void findByIdTest() {

		testData.removeAllInstancesFromDatabase();

		// insert 10 instances
		for (int i = 0; i < 10; i++) {
			testData.createInstance();
		}

		// get all instances from database
		List<Instance> instances = new DAO<Instance>().findAll(Instance.class);

		// assert that each instance retrieved by the web service is exactly
		// equal to the instance retrieved by the DAO
		for (Instance instance : instances) {
			Instance retrievedInstance = target.path("instance/" + instance.getId()).request().get(Instance.class);
			assertEquals(instance, retrievedInstance);
		}

	}

	private void assertEquals(Instance instance, Instance retrievedInstance) {
		// assert that retrieved instance attributes are equal to original
		// instance
		Assert.assertEquals(instance.getHostname(), retrievedInstance.getHostname());
		Assert.assertEquals(instance.getDescription(), retrievedInstance.getDescription());
		Assert.assertEquals(instance.getAutomaticRestart(), retrievedInstance.getAutomaticRestart());
		Assert.assertEquals(instance.getPreemtible(), retrievedInstance.getPreemtible());
		Assert.assertEquals(instance.getZone().getId(), retrievedInstance.getZone().getId());
		Assert.assertEquals(instance.getMachineType().getId(), retrievedInstance.getMachineType().getId());
		// assert that disk ids from retrieved instance are equal to the
		// ones
		// from original instance
		List<Disk> disks = instance.getDisks();
		for (int i = 0; i < disks.size(); i++) {
			Assert.assertEquals(disks.get(i).getId(), retrievedInstance.getDisks().get(i).getId());
		}

		// assert that network interface ids from retrieved instance are
		// equal
		// to the ones from original instance
		List<NetworkInterface> networkInterfaces = instance.getNetworkInterfaces();
		for (int i = 0; i < networkInterfaces.size(); i++) {
			Assert.assertEquals(networkInterfaces.get(i).getId(),
					retrievedInstance.getNetworkInterfaces().get(i).getId());
		}

		// assert that ssh keys ids from retrieved instance are equal to the
		// ones from original instance
		List<SSHKey> sshKeys = instance.getSshKeys();
		for (int i = 0; i < sshKeys.size(); i++) {
			Assert.assertEquals(sshKeys.get(i).getId(), retrievedInstance.getSshKeys().get(i).getId());
		}
	}

	@AfterClass
	public static void clearDatabase() {
		testData.clearTestDataFromDatabase();
	}
}
