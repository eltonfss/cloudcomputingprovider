package com.cloudprovider;

import java.util.ArrayList;
import java.util.List;

import com.cloudprovider.model.entity.Disk;
import com.cloudprovider.model.entity.Instance;
import com.cloudprovider.model.entity.MachineType;
import com.cloudprovider.model.entity.NetworkInterface;
import com.cloudprovider.model.entity.Region;
import com.cloudprovider.model.entity.SSHKey;
import com.cloudprovider.model.entity.Zone;
import com.cloudprovider.model.persistence.DAO;

public class TestData {

	private List<Disk> disks;
	private MachineType machineType;
	private List<NetworkInterface> networkInterfaces;
	private List<SSHKey> sshKeys;
	private Zone zone;
	private Instance instance;

	public TestData() {
	}

	public void clearTestDataFromDatabase() {
		removeAllInstancesFromDatabase();
		new DAO<Disk>().removeAll(Disk.class);
		new DAO<NetworkInterface>().removeAll(NetworkInterface.class);
		new DAO<MachineType>().removeAll(MachineType.class);
		new DAO<Zone>().removeAll(Zone.class);
		new DAO<Region>().removeAll(Region.class);
		new DAO<SSHKey>().removeAll(SSHKey.class);
	}

	public void removeAllInstancesFromDatabase() {
		new DAO<Instance>().removeAll(Instance.class);
	}

	public void createInstance() {
		instance = getNewInstance();
		DAO<Instance> instanceDAO = new DAO<Instance>();
		instanceDAO.create(instance);
	}

	public Instance getNewInstance() {
		Instance instance = new Instance(true);
		instance.setAutomaticRestart(false);
		instance.setDescription("Test description" + Math.random() * 1000);
		instance.setDisks(disks);
		instance.setHostname("Test hostname " + Math.random() * 1000);
		instance.setMachineType(machineType);
		instance.setName("Test name" + +Math.random() * 1000);
		instance.setNetworkInterface(networkInterfaces);
		instance.setOnHostMaintenance(false);
		instance.setSshKeys(sshKeys);
		instance.setZone(zone);
		return instance;
	}

	public void createDisks() {
		disks = new ArrayList<>();
		disks.add(new Disk(null, "Disk1"+Math.random()*10, 250.00));
		disks.add(new Disk(null, "Disk2"+Math.random()*10, 350.00));
		disks.add(new Disk(null, "Disk3"+Math.random()*10, 150.00));
		disks.add(new Disk(null, "Disk4"+Math.random()*10, 350.00));
		disks.add(new Disk(null, "Disk5"+Math.random()*10, 250.00));

		DAO<Disk> diskDAO = new DAO<>();
		for (Disk disk : disks) {
			diskDAO.create(disk);
		}

		disks = diskDAO.findAll(Disk.class);
	}

	public void createMachineType() {
		machineType = new MachineType();
		machineType.setMaxNumberOfPersistentDisks(3);
		machineType.setMaxTotalPersistentDiskSize(200.00);
		machineType.setMemory(8.0);
		machineType.setName("Machine Type 1"+Math.random()*10);
		machineType.setVirtualCPUS(8);

		DAO<MachineType> machineTypeDAO = new DAO<>();
		machineTypeDAO.create(machineType);

		machineType = machineTypeDAO.findAll(MachineType.class).get(0);
	}

	public void createNetworkInterfaces() {
		networkInterfaces = new ArrayList<>();
		networkInterfaces.add(new NetworkInterface(null, "08-89-9B-89-C8-F0", "Network Interface 1"+Math.random()*10, "200.232.77.149",
				"200.232.77.149", "9080:19fd:2ec9:af77:9775:eef3:29e0:a330",
				"c349:ad31:9ef0:e68e:c0fd:fc6e:ec55:e4b0"));
		networkInterfaces.add(new NetworkInterface(null, "5F-2B-EC-AA-BC-26",
				"Network Interface 2" + Math.random() * 10, "19.29.22.180",
				"56.125.141.194", "a3c7:e0a0:19b1:f050:e47d:4dea:500d:51ce", "a89e:89cc:c29f:d3a9:94a5:149d:9594:d44"));

		DAO<NetworkInterface> networkInterfaceDAO = new DAO<>();
		for (NetworkInterface networkInterface : networkInterfaces) {
			networkInterfaceDAO.create(networkInterface);
		}

		networkInterfaces = networkInterfaceDAO.findAll(NetworkInterface.class);
	}

	public void createSSHKeys() {
		sshKeys = new ArrayList<>();
		sshKeys.add(new SSHKey(null,
				"MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDXIVuBfYKLXDseOyFUfPnMCKYM9PteXZqPerVI/27SE8Kv3uf/Nc8Z9ZYRmj595Lqy3u0vsmEGa7+xm7kwmNYcksA7WsrXnUEDP/y2uqfKn0CUfLkfsuZcd76clpVCnJQLhsRtlPgtFTvUs+8AeCJIV8G3nbjAPxRMJfkaqq/t/wIDAQAB"));
		sshKeys.add(new SSHKey(null,
				"MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQChBfIAWx6Rr0+MaAhmC1k4O8RD4Ro8PIrVbawMhXUBf7XOXEF+DzwRtP5EiC09YQ91sXFggKqGOORcSEHff7WIxDcyLcaN+2ImXbYvSiaRhKSW3lHe5ZZHRyrf1ys2Ao1vHZ6MdGktWi04a/Y50yfr3li91aPen9+a519eCut9cwIDAQAB"));

		DAO<SSHKey> sshKeyDAO = new DAO<>();
		for (SSHKey sshKey : sshKeys) {
			sshKeyDAO.create(sshKey);
		}

		sshKeys = sshKeyDAO.findAll(SSHKey.class);
	}

	public void createZone() {

		Region region = new Region(null, "Region 1");
		DAO<Region> regionDAO = new DAO<>();
		regionDAO.create(region);
		region = regionDAO.findAll(Region.class).get(0);

		zone = new Zone(null, "Zone 1", region);
		DAO<Zone> zoneDAO = new DAO<>();
		zoneDAO.create(zone);
		zone = zoneDAO.findAll(Zone.class).get(0);

	}

	public List<Disk> getDisks() {
		return disks;
	}

	public MachineType getMachineType() {
		return machineType;
	}

	public List<NetworkInterface> getNetworkInterfaces() {
		return networkInterfaces;
	}

	public List<SSHKey> getSshKeys() {
		return sshKeys;
	}

	public Zone getZone() {
		return zone;
	}

	public Instance getInstance() {
		return instance;
	}

}
