package com.cloudprovider.model.persistence;

import java.util.List;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.cloudprovider.TestData;
import com.cloudprovider.model.entity.Disk;
import com.cloudprovider.model.entity.Instance;
import com.cloudprovider.model.entity.NetworkInterface;
import com.cloudprovider.model.entity.SSHKey;

public class DAOTest {

	private static TestData testData;

	@BeforeClass
	public static void initializeTestData() {
		testData = new TestData();
		testData.createDisks();
		testData.createMachineType();
		testData.createNetworkInterfaces();
		testData.createSSHKeys();
		testData.createZone();
	}

	@Test
	public void createInstanceTest() {

		// create instance
		testData.createInstance();
		Instance instance = testData.getInstance();

		// retrieve from database
		DAO<Instance> instanceDAO = new DAO<>();
		Instance retrievedInstance = instanceDAO.findAll(Instance.class).get(0);

		// assert that retrieved instance attributes are equal to original
		// instance
		Assert.assertEquals(instance.getHostname(), retrievedInstance.getHostname());
		Assert.assertEquals(instance.getDescription(), retrievedInstance.getDescription());
		Assert.assertEquals(instance.getAutomaticRestart(), retrievedInstance.getAutomaticRestart());
		Assert.assertEquals(instance.getPreemtible(), retrievedInstance.getPreemtible());
		Assert.assertEquals(instance.getZone().getId(), retrievedInstance.getZone().getId());
		Assert.assertEquals(instance.getMachineType().getId(), retrievedInstance.getMachineType().getId());

		// assert that disk ids from retrieved instance are equal to the ones
		// from original instance
		List<Disk> disks = instance.getDisks();
		for (int i = 0; i < disks.size(); i++) {
			Assert.assertEquals(disks.get(i).getId(), retrievedInstance.getDisks().get(i).getId());
		}

		// assert that network interface ids from retrieved instance are equal
		// to the ones from original instance
		List<NetworkInterface> networkInterfaces = instance.getNetworkInterfaces();
		for (int i = 0; i < networkInterfaces.size(); i++) {
			Assert.assertEquals(networkInterfaces.get(i).getId(),
					retrievedInstance.getNetworkInterfaces().get(i).getId());
		}

		// assert that ssh keys ids from retrieved instance are equal to the
		// ones from original instance
		List<SSHKey> sshKeys = instance.getSshKeys();
		for (int i = 0; i < sshKeys.size(); i++) {
			Assert.assertEquals(sshKeys.get(i).getId(), retrievedInstance.getSshKeys().get(i).getId());
		}

	}

	@AfterClass
	public static void clearDatabase() {
		testData.clearTestDataFromDatabase();
	}

}
